import java.util.Scanner;
public class task_446 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int count = 0;
        for (int i = 1; i < n; i++) {
            if ((n/i)==(n%i))
                count++;
        }
        System.out.println(count);
    }
}
