import java.util.Scanner;

public class task_108 {
    public static int medianNumber(int a, int b, int c){
        if ((a>=b && a<=c) || (a<=b && a>=c))
            return a;
        else if ((b>=a && b<=c)|| (b<=a && b>=c))
            return b;
        else
            return c;
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int a = scan.nextInt();
        int b = scan.nextInt();
        int c = scan.nextInt();

        System.out.println(medianNumber(a,b,c));

    }

}
