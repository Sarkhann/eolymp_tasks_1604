import java.util.Scanner;
public class task_248 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int half = (n*(n+1))/2;
        System.out.println(2*half + 1);
    }
}
