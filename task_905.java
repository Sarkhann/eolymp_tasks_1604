import java.util.Scanner;

public class task_905 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        long n = scan.nextLong();
        long m = scan.nextLong();
        long k = scan.nextLong();
        if (n == m && n == k && m == k) {
            System.out.println("1");
        } else if (n != m && n != k && m != k) {
            System.out.println("3");
        } else {
            System.out.println("2");
        }
    }
}