import java.util.Scanner;

public class task_1610 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int m = scan.nextInt();
        if (m >= n) {
            if (m % n != 0) {
                System.out.println(m / n + 1);
            }
            else
                System.out.println(m / n);
        }
        else
            System.out.println("1");
    }
}
