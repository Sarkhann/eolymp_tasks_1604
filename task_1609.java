import java.util.Scanner;

public class task_1609 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        n = Math.abs(n);
        int a = scan.nextInt();
        int count = 0;
        while (n>0){
            if (n%10==a)
                count++;
            n/=10;
        }
        System.out.println(count);
    }
}
