import java.util.Scanner;

public class task_107 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int money100 = 100, money20 = 20;
        int diskCount100 = n / money100;
        int diskCount20 = (n % money100) / money20;
        int diskCount1 = (n % money100) % money20;
        if (diskCount20>=3) {
            if (diskCount1 >= 5)
                System.out.println(diskCount100 * 100 + 100);
            else if (diskCount20==4 && diskCount1<=5)
                System.out.println(diskCount100 *100 + 100 );
            else
                System.out.println(diskCount100 *100 + diskCount20 * 30 +diskCount1*2 );
        }
        else if (diskCount1>15 && diskCount1<20)
            System.out.println(diskCount100 * 100 + diskCount20 * 30 + 30);
        else
            System.out.println(diskCount100 * 100 + diskCount20 * 30 + diskCount1 * 2);
    }

}
