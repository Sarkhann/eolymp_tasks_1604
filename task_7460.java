import java.util.Scanner;

public class task_7460 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        int m = scan.nextInt();
        int k = scan.nextInt();
        if (n % k == 0 && m % k == 0) {
            System.out.println((n + m) / k);
        }
        else if ((n % k != 0 && m % k == 0 || (n % k == 0 && m % k != 0)))
            System.out.println(((n / k + m/ k) + 1));
        else
            System.out.println(((n / k + m/ k) + 2));
    }
}
